//
//  iOS Zappos API Acceleration sample
//  Copyright © 2018 Zycada Networks. All rights reserved.
//

import UIKit

class ViewController: UIViewController, URLSessionTaskDelegate, URLSessionDataDelegate {

    //MARK: Properties
    var UUID = "" //UUID is used as session identifier for push to happen
    
    @IBOutlet weak var ghostButtonOut: UIButton! //ghost button to start ghost call
    @IBOutlet weak var runButtonOut: UIButton! //run button will trigger api search call
    
    @IBAction func ghostCall(_ sender: Any) {
        UUID = randomString(length: 8)
        //let ghostUrl = URL(string: "https://zycada-amzn.zappos.com/zycadize?X-Zy-UUID=" + UUID)!
        let ghostUrl = URL(string: "https://zycada-amzn-zappos-com.zycadize.com/zycadize?X-Zy-UUID=" + UUID)!
        let ghostUrlReq = URLRequest(url: ghostUrl)
        let ghostTask = URLSession.shared.dataTask(with: ghostUrlReq) { (data, response, error) in
            guard error == nil else {
                NSLog("error=\(error!) \n")
                return
            }
            NSLog("done request to =\(ghostUrl) \n")
        }
        ghostTask.resume()
        
    }
    
    @IBAction func runButton(_ sender: UIButton) {
        // do search api and parse all assets to push from JSON response
        //let fetchUrl = URL(string: "https://zycadaapi.zappos.com/v2/Search/null/filter/txAttrFacet_Gender/%22Women%22/zc3/%22Lifestyle%20Sneakers%22/zc2/%22Sneakers%20%26%20Athletic%20Shoes%22/zc4/%22Casual%20and%20Fashion%20Sneakers%22/zc1/%22Shoes%22/hc_women_size/%2211%20OR%2011.5%20OR%2012%22/sort/brandNameFacet/asc/limit/25/page/1?key=29fa9e681f15a44e37ab1d9b12a6a26548c6ded3&includes=%5B%22onSale%22,%22isNew%22,%22facets%22,%22productRating%22,%22msaImageId%22,%22facetPrediction%22,%22currentPage%22%5D&excludes=%5B%22productUrl%22%5D")!
        let fetchUrl = URL(string: "https://zycadaapi-zappos-com.zycadize.com/Search?sort=%7B%22productPopularity%22%3A%22asc%22%7D&filters=%7B%22txAttrFacet_Gender%22%3A%5B%22Women%22%5D%7D&includes=%5B%22msaImageId%22%2C%22limit%22%2C%22currentPage%22%2C%22colorId%22%2C%22onSale%22%2C%22isNew%22%2C%22facets%22%2C%22productRating%22%5D&limit=25&tl=true&excludes=%5B%22productUrl%22%5D&page=1&key=29fa9e681f15a44e37ab1d9b12a6a26548c6ded3&term=shoes")!
        var fetchUrlReq = URLRequest(url: fetchUrl)
        fetchUrlReq.setValue(UUID, forHTTPHeaderField: "X-Zy-UUID")
        let fetchTask = URLSession.shared.dataTask(with: fetchUrlReq) { (data, response, error) in
            guard error == nil else {
//                NSLog("when fetching \(fetchUrl) got error=\(error!) \n")
                return
            }
//            NSLog("completed request to =\(fetchUrl) \n")
            var assetsToPushArr = [String]()
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
                    let results = json["results"] as? [[String: Any]] ?? []
                    
                    for r in results {
                        let msaId = r["msaImageId"] as? String ?? ""
                        //let assetUrl = "https://zycada-amzn.zappos.com" + "/images/I/" + msaId + "._SX240_.jpg"
                        let assetUrl = "https://zycada-amzn-zappos-com.zycadize.com" + "/images/I/" + msaId + "._SX240_.jpg"
                        assetsToPushArr.append(assetUrl)
                    }
                }
                
                catch let error as NSError{
                    NSLog(error.localizedDescription)
                    return
                }
            }
                
            else {
                NSLog("data is empty \n")
                return
            }
            
            sleep(10)
            
            self.loadImages(paths: assetsToPushArr)
    
        }
        fetchTask.resume()
    }
    
    func loadImages(paths: [String]) {
        // load all images inside the array to UI frame
        for i in 0..<paths.count {
            let assetUrl = URL(string: paths[i])!
            var assetUrlReq = URLRequest(url: assetUrl)
            
            let assetTask = URLSession.shared.dataTask(with: assetUrlReq) { (data, response, error) in
                guard error == nil else {
                    NSLog("when fetching \(assetUrl) got error=\(error!) \n")
                    return
                }
                if let res = response as? HTTPURLResponse {
                    
                    // Start background thread so that image loading does not make app unresponsive
                    DispatchQueue.global(qos: .userInitiated).async {
                        if let imageData = data {
                            NSLog("Succefully fetched image \(assetUrl) with status code \(res.statusCode)")
                            
                            let imageView = UIImageView(frame: CGRect(x:50+i%6*50, y:250+i/6*50, width:50, height:50))
                            // When from background thread, UI needs to be updated on main_queue
                            DispatchQueue.main.async {
                                let image = UIImage(data: imageData)
                                imageView.image = image
                                imageView.contentMode = UIViewContentMode.scaleAspectFit
                                self.view.addSubview(imageView)
                            }
                            
                        } else {
                            NSLog("Couldn't get image from \(assetUrl) : Image is nil")
                        }
                    }
                }
            }
            assetTask.resume()
            
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didFinishCollecting metrics: URLSessionTaskMetrics)  {
        let interval = metrics.taskInterval
        let url = task.originalRequest?.url
        NSLog("Request to \(url) finished in \(interval)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func randomString(length: Int) -> String {
        // return random string for UUID
        let letters : NSString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }

}

